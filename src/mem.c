#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    size_t region_size = region_actual_size(query);
    void *region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    struct region res = {0};
    if (region_addr == MAP_FAILED) {
        return REGION_INVALID;
    }
    block_init(region_addr, (block_size) {.bytes = region_size}, NULL);
    res.addr = region_addr;
    res.size = region_size;
    res.extends = true;
    return res;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

void print_block_header(struct block_header *header) {
    printf("Block Header Information:\n");
    printf("Next: %s\n", header->next ? "NON_NULL" : "NULL");
    printf("Capacity: %zu\n", header->capacity.bytes);
    printf("Is Free: %s\n", header->is_free ? "true" : "false");
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block == NULL || !block_splittable(block, query)) return false;
    block_size needed_area_size = size_from_capacity((block_capacity) {query});
    block_size unneeded_area_size = {size_from_capacity(block->capacity).bytes - needed_area_size.bytes};
    block->capacity.bytes = query;
    void *unneeded_half_of_block = block_after(block);
    block_init(unneeded_half_of_block, unneeded_area_size, block->next);
    block->next = unneeded_half_of_block;
    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static inline void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (block == NULL) return false;
    struct block_header *maybe_new_block = block->next;
    if (maybe_new_block == NULL || !mergeable(block, maybe_new_block)) {
        return false;
    }
    block->next = maybe_new_block->next;
    block->capacity.bytes += size_from_capacity(maybe_new_block->capacity).bytes;
    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header *block_iterator = block;
    struct block_search_result res = {BSR_REACHED_END_NOT_FOUND, NULL};
    while (block_iterator != NULL) {
        while (try_merge_with_next(block_iterator));
        if ((block_iterator->is_free) && (block_is_big_enough(sz, block_iterator))) {
            res.type = BSR_FOUND_GOOD_BLOCK;
            res.block = block_iterator;
            return res;
        }
        if (block_iterator->next == NULL) break;
        block_iterator = block_iterator->next;
    }
    res.block = block_iterator;
    return res;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result first_good_or_last = find_good_or_last(block, query);
    if (first_good_or_last.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(first_good_or_last.block, query);
        first_good_or_last.block->is_free = false;
    }
    return first_good_or_last;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL) return NULL;
    void *init_block = block_after(last);
    struct region new_region = alloc_region(init_block, query);
    struct block_header *grow_block = new_region.addr;
    last->next = grow_block;
    return grow_block;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (heap_start == NULL) return NULL;
    size_t real_query = size_max(BLOCK_MIN_CAPACITY, query);
    struct block_search_result raw_res = try_memalloc_existing(real_query, heap_start);
    switch (raw_res.type) {
        case BSR_FOUND_GOOD_BLOCK:
            return raw_res.block;
        case BSR_REACHED_END_NOT_FOUND:
            grow_heap(raw_res.block, real_query);
            if (raw_res.block->next == NULL) return NULL;
            struct block_search_result res = try_memalloc_existing(real_query, heap_start);
            if (res.type == BSR_FOUND_GOOD_BLOCK) return res.block;
            break;
        default:
            return NULL;
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(((query + 15) >> 4) << 4, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
