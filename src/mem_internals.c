//
// Created by Vasilii on 26.01.2024.
//

#include "mem_internals.h"

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}