//
// Created by Vasilii on 24.01.2024.
//

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <stdio.h>

#define TEST_1_HEAP_SIZE REGION_MIN_SIZE
#define TEST_1_ALLOCATED_SIZE 250

#define TEST_2_HEAP_SIZE REGION_MIN_SIZE
#define TEST_2_ALLOCATED_1_SIZE 250
#define TEST_2_ALLOCATED_2_SIZE 500

#define TEST_3_HEAP_SIZE REGION_MIN_SIZE
#define TEST_3_ALLOCATED_1_SIZE 250
#define TEST_3_ALLOCATED_2_SIZE 500
#define TEST_3_ALLOCATED_3_SIZE 750

#define TEST_4_HEAP_SIZE REGION_MIN_SIZE
#define TEST_4_ALLOCATED_SIZE 10000

#define TEST_5_HEAP_SIZE REGION_MIN_SIZE
#define TEST_5_ALLOCATED_1_SIZE REGION_MIN_SIZE
#define TEST_5_ALLOCATED_2_SIZE 1000

static inline void clear_heap(void *heap, size_t size) {
    munmap(heap, size);
}

//Обычное успешное выделение памяти
int8_t test1() {
    fprintf(stdout, "Test 1 Started...\n");
    struct block_header *heap = heap_init(TEST_1_HEAP_SIZE);
    void *malloc_res = _malloc(TEST_1_ALLOCATED_SIZE);
    if (malloc_res == NULL || block_get_header(malloc_res)->is_free == true) {
        fprintf(stderr, "Test 1 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_1_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res);
    fprintf(stdout, "Test 1 PASSED\n");
    return 1;
}

//Освобождение одного блока из нескольких выделенных
int8_t test2() {
    fprintf(stdout, "Test 2 Started...\n");
    struct block_header *heap = heap_init(TEST_2_HEAP_SIZE);
    void *malloc_res_1 = _malloc(TEST_2_ALLOCATED_1_SIZE);
    void *malloc_res_2 = _malloc(TEST_2_ALLOCATED_2_SIZE);
    if (malloc_res_1 == NULL || malloc_res_2 == NULL || block_get_header(malloc_res_1)->is_free == true ||
        block_get_header(malloc_res_2)->is_free == true) {
        fprintf(stderr, "Test 2 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_2_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res_1);
    if (block_get_header(malloc_res_1)->is_free == false || block_get_header(malloc_res_2)->is_free == true) {
        fprintf(stderr, "Test 2 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_2_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res_2);
    fprintf(stdout, "Test 2 PASSED\n");
    return 1;
}

//Освобождение двух блоков из нескольких выделенных
int8_t test3() {
    fprintf(stdout, "Test 3 Started...\n");
    struct block_header *heap = heap_init(TEST_3_HEAP_SIZE);
    void *malloc_res_1 = _malloc(TEST_3_ALLOCATED_1_SIZE);
    void *malloc_res_2 = _malloc(TEST_3_ALLOCATED_2_SIZE);
    void *malloc_res_3 = _malloc(TEST_3_ALLOCATED_3_SIZE);
    if (malloc_res_1 == NULL || malloc_res_2 == NULL || malloc_res_3 == NULL) {
        fprintf(stderr, "Test 3 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_3_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res_1);
    _free(malloc_res_2);
    if (block_get_header(malloc_res_1)->is_free == false || block_get_header(malloc_res_2)->is_free == false ||
        block_get_header(malloc_res_3)->is_free == true) {
        fprintf(stderr, "Test 3 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_3_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res_2);
    fprintf(stdout, "Test 3 PASSED\n");
    return 1;
}

//Память закончилась, новый регион памяти расширяет старый
int8_t test4() {
    fprintf(stdout, "Test 4 Started...\n");
    struct block_header *heap = heap_init(TEST_4_HEAP_SIZE);
    void *malloc_res = _malloc(TEST_4_ALLOCATED_SIZE);
    if (malloc_res == NULL || block_get_header(malloc_res)->is_free == true) {
        fprintf(stderr, "Test 4 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_4_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res);
    fprintf(stdout, "Test 4 PASSED\n");
    return 1;
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
int8_t test5() {
    fprintf(stdout, "Test 5 Started...\n");
    struct block_header *heap = heap_init(TEST_5_HEAP_SIZE);
    void *malloc_res_1 = _malloc(TEST_5_ALLOCATED_1_SIZE);
    if (malloc_res_1 == NULL || block_get_header(malloc_res_1)->is_free == true) {
        fprintf(stderr, "Test 5 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_5_HEAP_SIZE);
        return 0;
    }
    void *malloc_res_1_addr = heap;
    void *change = mmap(malloc_res_1_addr, TEST_5_HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
    void *malloc_res_2 = _malloc(TEST_5_ALLOCATED_2_SIZE);
    if (malloc_res_2 == NULL || block_get_header(malloc_res_2)->is_free == true || heap == change) {
        fprintf(stderr, "Test 5 FAILED\n");
        debug_heap(stdout, heap);
        clear_heap(heap, TEST_5_HEAP_SIZE);
        return 0;
    }
    _free(malloc_res_1);
    _free(malloc_res_2);
    fprintf(stdout, "Test 5 PASSED\n");
    return 1;
}

int main() {
    int8_t test1_res = test1();
    int8_t test2_res = test2();
    int8_t test3_res = test3();
    int8_t test4_res = test4();
    int8_t test5_res = test5();
    printf("TESTS PASSED: %" PRId8  "/5\n", test1_res + test2_res + test3_res + test4_res + test5_res);
}

